
$(document).ready(function () {
	$("body").css({'visibility': "visible", "opacity": "1"});
	// forms();

	var swiper = new Swiper('.-slider', {
		spaceBetween: 30,
		navigation: {
			nextEl: '.slider-next',
			prevEl: '.slider-prev',
		},
		pagination: {
			el: '.first-pagination',
		},
	});
	new WOW().init();
	var swiper = new Swiper('.exp-slider', {
		spaceBetween: 30,
		slidesPerView: 3,
		navigation: {
			nextEl: '.exp-next',
			prevEl: '.exp-prev',
		},
		breakpoints: {
			767: {
				slidesPerView: 2,
			},
			599: {
				slidesPerView: 1,
			},
		}
	});
	var swiper = new Swiper('.exp-slider-1', {
		spaceBetween: 30,
		slidesPerView: 3,
		navigation: {
			nextEl: '.exp-next1',
			prevEl: '.exp-prev1',
		},
		breakpoints: {
			767: {
				slidesPerView: 2,
			},
			599: {
				slidesPerView: 1,
			},
		}
	});

	$(".header__mobile").click(function () {
		$(".header__menu, .header__menu-overlay").addClass("active");
	});
	$(".header__close, .header__menu-overlay").click(function() {
		$(".header__menu, .header__menu-overlay").removeClass("active");
	});
	$('.form-bottom').each(function() {
        var it = $(this);
         it.validate({
			rules: {
				email: {
					required: true,
					email: true,
				},
				name: {
					required: true,
				},
				company: {
					required: true,
				},
				phone: {
					required: true,
					digits: true,
				},
				message: {
					required: true,
				}

			},

			errorPlacement: function (error, element) {
			},

			submitHandler: function() {
				// if ($(".contact__bottom input[name=check]").is(":checked")) {
					$.ajax({
						success: function(){
							it.find("input, textarea").val('');
						}
					});
				// }
			},  
         });
	 });
	var swiper = new Swiper('.ev-slider', {
		spaceBetween: 30,
		slidesPerView: 5,
		navigation: {
			nextEl: '.ev-next',
			prevEl: '.ev-prev',
		},
		breakpoints: {
			1024: {
				slidesPerView: 3,
			},
			768: {
				slidesPerView: 2,
			},
			599: {
				slidesPerView: 1,
			},
		}
	});

	
	$(".sticky-block").stick_in_parent();
	if ($(window).width() <= 1025) {
		var prodhtml = $(".prod__content .prod__left").remove();
		$(".prod__top").append(prodhtml);
	}

	
	if ($(window).width() <= 1025) {
		var prodhtml1 = $(".tab-desc-1:nth-child(2)").remove();
		$(".tab1 .prod__content").find(".wrapper").append(prodhtml1);
		// var prodhtml2 = $(".tab-desc-2").remove();
		var height = $(".tab-desc-2:nth-child(2)").height();
		// $('.prod__desc').css("height", height);
		$(window).resize(function() {
			var height = $(".tab-desc-2:nth-child(2)").height();
			// $('.prod__desc').css("height", height);
		});
	}

	$(".sticky-block").stick_in_parent();
	if ($(window).width() <= 1025) {
		var prodhtml1 = $(".tab-desc-2:nth-child(2)").remove();
		$(".tab2 .prod__content").find(".wrapper").append(prodhtml1);
		// var prodhtml2 = $(".tab-desc-2").remove();
	}






	if ($(window).width() <= 425) {
		var footerCont = $(".footer__top-right a").remove();
		$(".top-wrapper").append(footerCont);
	}

	$(document).ready(function() {
		$('select').niceSelect();
	});

	$(".lightgallery").lightGallery();

	$(".m-bg-cont").each(function() {
		var img = $(this).find("img:first-of-type").attr("src");
		$(this).css("background-image", "url(" + img + ")");
	});
	$(".tabs").each(function() {
        $('.tab').click(function(e) {
            e.preventDefault();
            var it = $(this);
            var href = it.attr("href");
            $(".tab").removeClass("active");
            it.addClass("active");
            $(".tab[href="+href+"]").addClass("active")
            $(".cont-tab").removeClass("active");
            $("." + href).each(function () {
                $("." + href).addClass('active');
                $("." + href + " input").val('');
                $("." + href + " input").removeClass("is-focus");
            });
            window.scrollTo(window.scrollX, window.scrollY - 0.2);
            window.scrollTo(window.scrollX, window.scrollY + 0.2);

            
            $(".prod__left").removeClass("wow animated animate__fadeInRight1 fadeInLeft");
			$(".prod__items").removeClass("wow animated animate__fadeInLeft1 fadeInRight");

			$(".prod__content-container." + href + " .prod__left").addClass("wow animated animate__fadeInLeft1 fadeInLeft");
			$(".prod__content-container." + href + " .prod__items").addClass("wow animated animate__fadeInRight1 fadeInRight");
			clearInterval(myTimer);
			myTimer = setInterval(myMethod, 10000);
        });
	});	
	var coutRandom1 = Math.floor(Math.random() * 1500) + 500;    
	var string = $(".count__title-1").text();
	$(".count__title-1").text("");
	var timeCompare = new Date().getTime();
	var initTime = 1590505836463;
	day = Math.floor((timeCompare - initTime) / (1000 * 60 * 60 * 24));
	var toAdd = day * 1500;
	string = string.replace(/\d+( \d+)+/g, (match) => {
		match = match.replace(/ /g, "");
		match = +match + toAdd + (day % 20 * 100 + 587);
		return match.toString();
	})
	string = string.trim();
	$(".count__title-1").attr("data-count", string); 
	var coutRandom2 = Math.floor(Math.random() * 35) + 15;    
	toAdd = day * 20; 
	var string = $(".count__title-2").text();
	$(".count__title-2").text("");
	string = string.replace(/\d+( \d+)+/g, (match) => {
		match = match.replace(/ /g, "");
		match = +match + toAdd + (day % 10 + 35);
		return match.toString();
	})
	string = string.trim();
	$(".count__title-2").attr("data-count", string);

	$(".prod").hover(function() {
		$(".prod__top a").addClass("animation-active");
	});

	$(".prod__item").hover(function(ev) {
		clearInterval(myTimer);
	}, function(ev){
		myTimer = setInterval(myMethod, 10000);
	});


	var myTimer = setInterval(myMethod, 10000);
	function myMethod( )
	{
		$('.prod-tab:not(.prod-tab.active)').trigger("click");

	}
	$('.prod-tab:not(.prod-tab.active)').trigger("click");

	$(".scrolltop").click(function(e) {
		e.preventDefault();
	});
	// var timer;
	// for (var i = 1; i > 999999; i++) {
	// 	clearTimeout(timer);
	// 	timer = setTimeout(function() {
	// 		console.log(10021);
	// 	}, 300);
	// }

	
	$(".header-tab").click(function() {
		var scrtop = $(".prod").offset().top;
		window.scrollTo(window.scrollX, scrtop);  
	});

	$(".scrolltop-dom").click(function() {
		var img = $(".dom").offset().top;
		window.scrollTo(window.scrollX, img);  
	});

	$(".scrolltop-ev").click(function() {
		var img = $(".ev").offset().top;
		window.scrollTo(window.scrollX, img);  
	});

	$(".scrolltop-form").click(function() {
		var img = $(".form").offset().top;
		window.scrollTo(window.scrollX, img);  
	});
	
	$(".tab-2").click(function() {
		$(".prod__icon").addClass("active");
	});
	$(".tab-1").click(function() {
		$(".prod__icon").removeClass("active");
	});

	var a = 0;	

	if ($(".count").length > 0) {
		var oTop = $('.count').offset().top - window.innerHeight +150;
		if (a == 0 && $(window).scrollTop() > oTop) {
			$('.counter-value').each(function() {
			var $this = $(this),
				countTo = $this.attr('data-count');
				var floor = Math.floor(Math.random() * 20) + 20;
				var floor2 = floor * 50;
				$({
					countNum: 1
				}).animate({
					countNum: countTo
				},
				{
					duration: floor2,
					easing: 'swing',
					step: function() {
						$this.text(Math.floor(this.countNum));
						var text = $this.text();
						text = text.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						$this.text(text);
					},
					complete: function() {
						$this.text(this.countNum);
						$this.text(Math.floor(this.countNum));
						var text = $this.text();
						text = text.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						$this.text(text);
					}
				});
			});
			a = 1;
		}	
		
		$(window).scroll(function() {
			
			var oTop = $('.count').offset().top - window.innerHeight +150;
			if (a == 0 && $(window).scrollTop() > oTop) {
				$('.counter-value').each(function() {
					var $this = $(this),
					countTo = $this.attr('data-count');
					var floor = Math.floor(Math.random() * 20) + 20;
					var floor2 = floor * 100;
					$({
						countNum: $this.text()
					}).animate({
						countNum: countTo
					},
	
					{
						duration: floor2,
						easing: 'swing',
						step: function() {
							$this.text(Math.floor(this.countNum));
							var text = $this.text();
							text = text.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
							$this.text(text);
						},
						complete: function() {
							$this.text(this.countNum);
							$this.text(Math.floor(this.countNum));
						var text = $this.text();
						text = text.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
						$this.text(text);
						}
					});
				});
				a = 1;
			}
	
		});
	}
	
})